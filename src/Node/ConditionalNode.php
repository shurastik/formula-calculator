<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContextInterface;
use Axp\Calculator\NodeInterface;

/**
 * Class ConditionalNode
 * @package Axp\Calculator\Node
 */
class ConditionalNode implements NodeInterface
{
    /**
     * @var NodeInterface
     */
    private $conditionalNode;
    /**
     * @var NodeInterface
     */
    private $trueBranchNode;
    /**
     * @var NodeInterface
     */
    private $falseBranchNode;

    /**
     * @param NodeInterface $conditionalNode
     * @param NodeInterface $trueBranchNode
     * @param NodeInterface $falseBranchNode
     */
    public function __construct(
        NodeInterface $conditionalNode,
        NodeInterface $trueBranchNode,
        NodeInterface $falseBranchNode
    ) {
        $this->conditionalNode = $conditionalNode;
        $this->trueBranchNode = $trueBranchNode;
        $this->falseBranchNode = $falseBranchNode;
    }

    /**
     * @param ExecutionContextInterface $context
     * @return double
     */
    public function evaluate(ExecutionContextInterface $context)
    {
        return $this->conditionalNode->evaluate($context)
            ? $this->trueBranchNode->evaluate($context)
            : $this->falseBranchNode->evaluate($context);
    }

    /**
     * @return NodeInterface
     */
    public function getConditionNode()
    {
        return $this->conditionalNode;
    }

    /**
     * @return NodeInterface
     */
    public function getFalseBranchNode()
    {
        return $this->falseBranchNode;
    }

    /**
     * @return NodeInterface
     */
    public function getTrueBranchNode()
    {
        return $this->trueBranchNode;
    }
}
