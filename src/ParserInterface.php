<?php
namespace Axp\Calculator;

/**
 * Interface ParserInterface
 * @package Axp\Calculator
 */
interface ParserInterface
{
    /**
     * @return NodeInterface
     */
    public function parse();
}
