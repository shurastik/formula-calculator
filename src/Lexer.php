<?php
namespace Axp\Calculator;

/**
 * Class Lexer
 * @package Axp\Calculator
 */
class Lexer implements LexerInterface
{
    /**
     * @var string
     */
    private $input;
    /**
     * @var double
     */
    private $numberValue;
    /**
     * @var string
     */
    private $identifierString;
    /**
     * @var string
     */
    private $lastChar = ' ';

    private $position = 0;
    /**
     * @param string $input
     */
    public function __construct($input)
    {
        $this->input = mb_strtolower($input);
    }

    /**
     * @return int
     */
    public function getNextToken()
    {
        $res = null;

        while ($this->lastChar === ' ') {
            $this->readNextChar();
        }

        if (ctype_alpha($this->lastChar)) {
            $this->identifierString = $this->lastChar;
            $this->readNextChar();
            while (ctype_alnum($this->lastChar) || $this->lastChar === '_') {
                $this->identifierString .= $this->lastChar;
                $this->readNextChar();
            }

            if ($this->identifierString === 'if') {
                $res = self::TOKEN_IF;
            } else {
                $res = self::TOKEN_VARIABLE;
            }

            return $res;
        } elseif (ctype_digit($this->lastChar) || $this->lastChar === '.') {
            $this->numberValue = '';
            do {
                $this->numberValue .= $this->lastChar;
                $this->readNextChar();
            } while (ctype_digit($this->lastChar) || $this->lastChar === '.');
            $this->numberValue = (double)$this->numberValue;

            return self::TOKEN_NUMBER;
        }

        if ($this->lastChar === -1) {
            return self::TOKEN_EOF;
        }

        if ($this->lastChar === ',') {
            $res = self::TOKEN_COMMA;
        } elseif ($this->lastChar === '(') {
            $res = self::TOKEN_LPAREN;
        } elseif ($this->lastChar === ')') {
            $res = self::TOKEN_RPAREN;
        } else {
            $res = $this->lastChar;
        }

        $this->readNextChar();

        return $res;
    }

    /**
     * @return int
     */
    private function readNextChar()
    {
        return isset($this->input[$this->position])
            ? ($this->lastChar = $this->input[$this->position++])
            : $this->lastChar = -1;
    }
    /**
     * @return mixed string
     */
    public function getIdentifierString()
    {
        return $this->identifierString;
    }

    /**
     * @return double
     */
    public function getNumberValue()
    {
        return $this->numberValue;
    }

    /**
     * @return string
     */
    public function getInput()
    {
        return $this->input;
    }
}
