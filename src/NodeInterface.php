<?php
namespace Axp\Calculator;

/**
 * Interface NodeInterface
 * @package Axp\Calculator
 */
interface NodeInterface
{
    /**
     * @param ExecutionContextInterface $context
     * @return double
     */
    public function evaluate(ExecutionContextInterface $context);
}
