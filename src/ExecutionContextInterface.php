<?php
namespace Axp\Calculator;

/**
 * Interface ExecutionContextInterface
 * @package Axp\Calculator
 */
interface ExecutionContextInterface
{
    /**
     * @param string $variableName
     * @return double
     */
    public function getVariableValue($variableName);

    /**
     * @param string $name
     * @param double $value
     * @return void
     */
    public function setVariableValue($name, $value);
}
