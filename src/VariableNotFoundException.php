<?php
namespace Axp\Calculator;

/**
 * Class VariableNotFoundException
 * @package Axp\Calculator
 */
class VariableNotFoundException extends \RuntimeException
{
}
