<?php
namespace Axp\Calculator;

/**
 * Class CalculatorTest
 * @package Axp\Calculator
 */
class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function simpleCalculation()
    {
        $calculator = new Calculator("1+1");
        $this->assertEquals(2.0, $calculator->calculate());
    }

    /**
     * @test
     */
    public function anotherSimpleCalculation()
    {
        $calculator = new Calculator("1+2");
        $this->assertEquals(3.0, $calculator->calculate());
    }

    /**
     * @test
     */
    public function testVariables()
    {
        $calculator = new Calculator("a-b");
        $calculator->setVariable("a", 2.0);
        $calculator->setVariable("b", 1.0);
        $this->assertEquals(1.0, $calculator->calculate());
    }

    /**
     * @test
     */
    public function testIfCondition()
    {
        $calculator = new Calculator("if(1+2,a,b)");
        $calculator->setVariable("a", 2.0);
        $calculator->setVariable("b", 1.0);
        $this->assertEquals(2.0, $calculator->calculate());
    }

    /**
     * @test
     */
    public function testParens()
    {
        $calculator = new Calculator("2*(2+3)");
        $this->assertEquals(10.0, $calculator->calculate());
    }

    /**
     * @test
     */
    public function testEquality()
    {
        $calculator = new Calculator("if(1=1,1,0)");
        $this->assertEquals(1.0, $calculator->calculate());
        $calculator = new Calculator("if(1=0,1,0)");
        $this->assertEquals(0.0, $calculator->calculate());
    }
}
