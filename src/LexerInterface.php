<?php
namespace Axp\Calculator;

/**
 * Interface LexerInterface
 * @package Axp\Calculator
 */
interface LexerInterface
{
    const TOKEN_EOF = -1;
    const TOKEN_IF = -2;
    const TOKEN_COMMA = -3;
    const TOKEN_LPAREN = -4;
    const TOKEN_RPAREN = -5;
    const TOKEN_VARIABLE = -6;
    const TOKEN_NUMBER = -7;

    /**
     * @return int
     */
    public function getNextToken();

    /**
     * @return mixed string
     */
    public function getIdentifierString();

    /**
     * @return double
     */
    public function getNumberValue();

    /**
     * @return string
     */
    public function getInput();
}
