<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContext;

/**
 * Class ConditionalNodeTest
 * @package Axp\Calculator\Node
 */
class ConditionalNodeTest extends \PHPUnit_Framework_TestCase
{
    public function testTrue()
    {
        $node = new ConditionalNode(
            new NumberNode(1),
            new NumberNode(2),
            new NumberNode(3)
        );
        $this->assertEquals(2, $node->evaluate(new ExecutionContext()));
    }
    public function testFalse()
    {
        $node = new ConditionalNode(
            new NumberNode(0),
            new NumberNode(2),
            new NumberNode(3)
        );
        $this->assertEquals(3, $node->evaluate(new ExecutionContext()));
    }
}
