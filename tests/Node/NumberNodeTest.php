<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContext;

/**
 * Class NumberNodeTest
 * @package Axp\Calculator\Node
 */
class NumberNodeTest extends \PHPUnit_Framework_TestCase
{
    public function testEvaluate()
    {
        $node = new NumberNode(1);
        $this->assertEquals(1, $node->evaluate(new ExecutionContext()));
    }
}
 