<?php
namespace Axp\Calculator;

/**
 * Class UnsupportedOperatorException
 * @package Axp\Calculator
 */
class UnsupportedOperatorException extends \RuntimeException
{
}
