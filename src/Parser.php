<?php
namespace Axp\Calculator;

use Axp\Calculator\Node\ConditionalNode;
use Axp\Calculator\Node\NumberNode;
use Axp\Calculator\Node\OperatorNode;
use Axp\Calculator\Node\VariableNode;

/**
 * Class Parser
 * @package Axp\Calculator
 */
class Parser implements ParserInterface
{
    /**
     * @var array
     */
    private static $precedence = array(
        '=' => 10,
        '<' => 10,
        '>' => 10,
        '+' => 20,
        '-' => 20,
        '*' => 40,
        '/' => 40,
    );
    /**
     * @var LexerInterface
     */
    private $lexer;
    /**
     * @var int
     */
    private $currentToken;

    /**
     * @param LexerInterface $lexer
     */
    public function __construct(LexerInterface $lexer)
    {
        $this->lexer = $lexer;
    }

    /**
     * @return int
     */
    private function getTokenPrecedence()
    {
        if (!isset(self::$precedence[$this->currentToken])) {
            return -1;
        }

        return self::$precedence[$this->currentToken];
    }

    /**
     * return @void
     */
    private function getNextToken()
    {
        $this->currentToken = $this->lexer->getNextToken();
    }

    /**
     * @return NodeInterface
     */
    public function parse()
    {
        $this->getNextToken();

        return $this->parseExpression();
    }

    /**
     * @return NumberNode
     */
    private function parseNumber()
    {
        $node = new NumberNode($this->lexer->getNumberValue());
        $this->getNextToken();

        return $node;
    }

    /**
     * @return NodeInterface
     * @throws SyntaxErrorException
     */
    private function parseExpression()
    {
        $node = $this->parsePrimary();

        return $this->parseBinaryRightNode(0, $node);
    }

    /**
     * @param int $precedence
     * @param NodeInterface $leftNode
     * @return NodeInterface
     * @throws SyntaxErrorException
     */
    private function parseBinaryRightNode($precedence, NodeInterface $leftNode)
    {
        while (true) {
            $tokenPrecedence = $this->getTokenPrecedence();
            if ($tokenPrecedence < $precedence) {
                break;
            }
            $binOperation = $this->currentToken;
            $this->getNextToken();
            $rightNode = $this->parsePrimary();
            $nextPrecedence = $this->getTokenPrecedence();
            if ($tokenPrecedence < $nextPrecedence) {
                $rightNode = $this->parseBinaryRightNode($tokenPrecedence + 1, $rightNode);
            }
            $leftNode = new OperatorNode($binOperation, $leftNode, $rightNode);
        }

        return $leftNode;
    }

    private function parseParens()
    {
        $this->getNextToken();
        $node = $this->parseExpression();
        if ($this->currentToken !== LexerInterface::TOKEN_RPAREN) {
            throw new SyntaxErrorException('Invalid parens');
        }
        $this->getNextToken();

        return $node;
    }

    /**
     * @return VariableNode
     */
    private function parseIdentifierExpr()
    {
        $idName = $this->lexer->getIdentifierString();
        $this->getNextToken();

        return new VariableNode($idName);
    }

    /**
     * @return ConditionalNode
     * @throws SyntaxErrorException
     */
    private function parseIfCondition()
    {
        $this->getNextToken();
        if ($this->currentToken !== LexerInterface::TOKEN_LPAREN) {
            throw new SyntaxErrorException('Invalid IF statement');
        }
        $this->getNextToken();
        $conditionNode = $this->parseExpression();
        if ($this->currentToken !== LexerInterface::TOKEN_COMMA) {
            throw new SyntaxErrorException('Invalid IF statement');
        }
        $this->getNextToken();
        $trueBranchNode = $this->parseExpression();
        if ($this->currentToken !== LexerInterface::TOKEN_COMMA) {
            throw new SyntaxErrorException('Invalid IF statement');
        }
        $this->getNextToken();
        $falseBranchNode = $this->parseExpression();
        if ($this->currentToken !== LexerInterface::TOKEN_RPAREN) {
            throw new SyntaxErrorException('Invalid IF statement');
        }
        $this->getNextToken();

        return new ConditionalNode($conditionNode, $trueBranchNode, $falseBranchNode);
    }

    /**
     * @return NodeInterface
     * @throws SyntaxErrorException
     */
    private function parsePrimary()
    {
        $res = null;
        switch ($this->currentToken) {
            case LexerInterface::TOKEN_NUMBER:
                $res = $this->parseNumber();
                break;
            case LexerInterface::TOKEN_IF:
                $res = $this->parseIfCondition();
                break;
            case LexerInterface::TOKEN_LPAREN:
                $res = $this->parseParens();
                break;
            case LexerInterface::TOKEN_VARIABLE:
                $res = $this->parseIdentifierExpr();
                break;
            default:
                throw new SyntaxErrorException('Syntax error');
        }

        return $res;
    }
}
