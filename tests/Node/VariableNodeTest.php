<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContext;

class VariableNodeTest extends \PHPUnit_Framework_TestCase
{
    public function testEvaluate()
    {
        $variableNode = new VariableNode('a');
        $context = new ExecutionContext(array('a' => 1));
        $this->assertEquals(1, $variableNode->evaluate($context));
    }

    public function testNotExistingVariableShouldThrowException()
    {
        $this->setExpectedException('Axp\Calculator\VariableNotFoundException');
        $variableNode = new VariableNode('b');
        $context = new ExecutionContext();
        $context->setVariableValue('a', 1);
        $this->assertEquals(1, $variableNode->evaluate($context));
    }
}
