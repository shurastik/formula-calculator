<?php
namespace Axp\Calculator;

use Axp\Calculator\Node\ConditionalNode;
use Axp\Calculator\Node\NumberNode;
use Axp\Calculator\Node\OperatorNode;
use Axp\Calculator\Node\VariableNode;

/**
 * Class ParserTest
 * @package Axp\Calculator
 */
class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $formula
     * @return NodeInterface
     */
    private function createNode($formula)
    {
        $lexer = new Lexer($formula);
        $parser = new Parser($lexer);

        return $parser->parse();
    }

    /**
     * @test
     */
    public function testParser()
    {
        /** @var OperatorNode $rootNode */
        $rootNode = $this->createNode("1+2");
        $this->assertTrue($rootNode instanceof OperatorNode);
        $this->assertTrue($rootNode->getLeftNode() instanceof NumberNode);
        $this->assertTrue($rootNode->getRightNode() instanceof NumberNode);
    }

    public function testPrecedence()
    {
        /** @var OperatorNode $rootNode */
        $rootNode = $this->createNode('1+2*3');
        $this->assertInstanceOf('Axp\Calculator\Node\OperatorNode', $rootNode);
        $this->assertEquals('+', $rootNode->getOperator());

        /** @var OperatorNode $rootNode */
        $rootNode = $this->createNode('2*3+1');
        $this->assertInstanceOf('Axp\Calculator\Node\OperatorNode', $rootNode);
        $this->assertEquals('+', $rootNode->getOperator());
    }

    public function testParen()
    {
        /** @var OperatorNode $rootNode */
        $rootNode = $this->createNode('(1+2)*3');
        $this->assertInstanceOf('Axp\Calculator\Node\OperatorNode', $rootNode);
        $this->assertEquals('*', $rootNode->getOperator());
    }

    public function testParensSyntaxError()
    {
        $this->setExpectedException('Axp\Calculator\SyntaxErrorException');
        $this->createNode('((1+2)*3');
    }

    public function testVariable()
    {
        /** @var VariableNode $rootNode */
        $rootNode = $this->createNode('a');
        $this->assertInstanceOf('Axp\Calculator\Node\VariableNode', $rootNode);
    }

    /**
     * @test
     */
    public function parseIfCondition()
    {
        /** @var ConditionalNode $rootNode */
        $rootNode = $this->createNode("if(1=2,1,2)");
        $this->assertTrue($rootNode instanceof ConditionalNode);
        $this->assertTrue($rootNode->getConditionNode() instanceof OperatorNode);
        $this->assertTrue($rootNode->getTrueBranchNode() instanceof NumberNode);
        $this->assertTrue($rootNode->getFalseBranchNode() instanceof NumberNode);
    }

    /**
     * @test
     */
    public function testNestedIf()
    {
        /** @var ConditionalNode $rootNode */
        $rootNode = $this->createNode("if(1,if(2,3,4),5)");
        $this->assertTrue($rootNode instanceof ConditionalNode);
        $this->assertTrue($rootNode->getTrueBranchNode() instanceof ConditionalNode);
    }

    public function testEmptyFormulaShouldThrowSyntaxError()
    {
        $this->setExpectedException('Axp\Calculator\SyntaxErrorException');
        $this->createNode('');
    }

    public function testInvalidIfSyntax()
    {
        try {
            $this->createNode('if 1,2,3)');
            $this->fail('fail on missing open paren');
        } catch (SyntaxErrorException $ex) {}
        try {
            $this->createNode('if(1 2,3)');
            $this->fail('fail on missing first comma');
        } catch (SyntaxErrorException $ex) {}
        try {
            $this->createNode('if(1,2 3)');
            $this->fail('fail on missing second comma');
        } catch (SyntaxErrorException $ex) {}
        try {
            $this->createNode('if(1,2,3');
            $this->fail('fail on missing right paren');
        } catch (SyntaxErrorException $ex) {}
    }
}
