<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContext;

/**
 * Class OperatorNodeTest
 * @package Axp\Calculator\Node
 */
class OperatorNodeTest extends \PHPUnit_Framework_TestCase
{
    public function testPlus()
    {
        $node = new OperatorNode(
            '+',
            new NumberNode(1),
            new NumberNode(2)
        );

        $this->assertEquals(3, $node->evaluate(new ExecutionContext()));
    }

    public function testMinus()
    {
        $node = new OperatorNode(
            '-',
            new NumberNode(5),
            new NumberNode(3)
        );

        $this->assertEquals(2, $node->evaluate(new ExecutionContext()));
    }

    public function testMul()
    {
        $node = new OperatorNode(
            '*',
            new NumberNode(5),
            new NumberNode(3)
        );

        $this->assertEquals(15, $node->evaluate(new ExecutionContext()));
    }

    public function testDiv()
    {
        $node = new OperatorNode(
            '/',
            new NumberNode(6),
            new NumberNode(3)
        );

        $this->assertEquals(2, $node->evaluate(new ExecutionContext()));
    }

    public function testEq()
    {
        $node = new OperatorNode(
            '=',
            new NumberNode(6),
            new NumberNode(3)
        );
        $this->assertEquals(0, $node->evaluate(new ExecutionContext()));

        $node = new OperatorNode(
            '=',
            new NumberNode(3),
            new NumberNode(3)
        );
        $this->assertEquals(1, $node->evaluate(new ExecutionContext()));
    }

    public function testUnsupportedOperator()
    {
        $this->setExpectedException('Axp\Calculator\UnsupportedOperatorException');

        $node = new OperatorNode(
            '%',
            new NumberNode(3),
            new NumberNode(3)
        );
        $node->evaluate(new ExecutionContext());
    }
}
