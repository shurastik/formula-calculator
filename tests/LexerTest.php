<?php
namespace Axp\Calculator;

/**
 * Class LexerTest
 * @package Vdi\Domain\OfferTool\Calculator
 */
class LexerTest extends \PHPUnit_Framework_TestCase
{
    public function testGetInput()
    {
        $lexer = new Lexer('test');
        $this->assertEquals('test', $lexer->getInput());
    }

    /**
     * @test
     */
    public function testTokens()
    {
        $formula = '+-*/if,test()   2.46 =asd_1';
        $lexer = new Lexer($formula);
        $this->assertEquals('+', $lexer->getNextToken());
        $this->assertEquals('-', $lexer->getNextToken());
        $this->assertEquals('*', $lexer->getNextToken());
        $this->assertEquals('/', $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_IF, $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_COMMA, $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_VARIABLE, $lexer->getNextToken());
        $this->assertEquals('test', $lexer->getIdentifierString());
        $this->assertEquals(LexerInterface::TOKEN_LPAREN, $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_RPAREN, $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_NUMBER, $lexer->getNextToken());
        $this->assertEquals(2.46, $lexer->getNumberValue());
        $this->assertEquals('=', $lexer->getNextToken());
        $this->assertEquals(LexerInterface::TOKEN_VARIABLE, $lexer->getNextToken());
        $this->assertEquals('asd_1', $lexer->getIdentifierString());
        $this->assertEquals(LexerInterface::TOKEN_EOF, $lexer->getNextToken());
    }
}
