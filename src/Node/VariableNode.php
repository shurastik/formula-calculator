<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContextInterface;
use Axp\Calculator\NodeInterface;

/**
 * Class VariableNode
 * @package Axp\Calculator
 */
class VariableNode implements NodeInterface
{
    /**
     * @var string
     */
    private $variableName;

    /**
     * @param string $variableName
     */
    public function __construct($variableName)
    {
        $this->variableName = $variableName;
    }

    /**
     * @param ExecutionContextInterface $context
     * @return double
     */
    public function evaluate(ExecutionContextInterface $context)
    {
        return $context->getVariableValue($this->variableName);
    }
}
