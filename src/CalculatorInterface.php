<?php
namespace Axp\Calculator;

/**
 * Interface CalculatorInterface
 * @package Axp\Calculator
 */
interface CalculatorInterface
{
    /**
     * @return double
     */
    public function calculate();

    /**
     * @param string $name
     * @param double $value
     * @return void
     */
    public function setVariable($name, $value);
}
