<?php
namespace Axp\Calculator;

/**
 * Class SyntaxErrorException
 * @package Axp\Calculator
 */
class SyntaxErrorException extends \RuntimeException
{
}
