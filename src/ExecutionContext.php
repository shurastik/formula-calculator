<?php
namespace Axp\Calculator;

/**
 * Class ExecutionContext
 * @package Axp\Calculator
 */
class ExecutionContext implements ExecutionContextInterface
{
    /**
     * @var array
     */
    private $values = array();

    /**
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        $this->values = $values;
    }

    /**
     * @param string $variableName
     * @throws VariableNotFoundException
     * @return double
     */
    public function getVariableValue($variableName)
    {
        if (!isset($this->values[$variableName])) {
            throw new VariableNotFoundException(sprintf("Variable '%s' value not found", $variableName));
        }
        return $this->values[$variableName];
    }

    /**
     * @param string $name
     * @param double $value
     * @return void
     */
    public function setVariableValue($name, $value)
    {
        $this->values[$name] = $value;
    }
}
