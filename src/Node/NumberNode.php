<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContextInterface;
use Axp\Calculator\NodeInterface;

/**
 * Class NumberNode
 * @package Axp\Calculator\Node
 */
class NumberNode implements NodeInterface
{
    /**
     * @var double
     */
    private $value;

    /**
     * @param double $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param ExecutionContextInterface $context
     * @return double
     */
    public function evaluate(ExecutionContextInterface $context)
    {
        return $this->value;
    }
}
