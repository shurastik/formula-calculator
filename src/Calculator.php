<?php
namespace Axp\Calculator;

/**
 * Class Calculator
 * @package Axp\Calculator
 */
class Calculator implements CalculatorInterface
{
    /**
     * @var NodeInterface
     */
    private $expression;
    /**
     * @var ExecutionContextInterface
     */
    private $context;

    /**
     * @param string $formula
     * @param ExecutionContextInterface $context
     * @param LexerInterface $lexer
     * @param ParserInterface $parser
     */
    public function __construct(
        $formula,
        ExecutionContextInterface $context = null,
        LexerInterface $lexer = null,
        ParserInterface $parser = null
    ) {
        $this->context = $context ?: new ExecutionContext();
        $lexer = $lexer ?: new Lexer($formula);
        $parser = $parser ?: new Parser($lexer);
        $this->expression = $parser->parse();
    }

    /**
     * @return double
     */
    public function calculate()
    {
        return $this->expression->evaluate($this->context);
    }

    /**
     * @param string $name
     * @param double $value
     * @return void
     */
    public function setVariable($name, $value)
    {
        $this->context->setVariableValue($name, $value);
    }
}
