<?php
namespace Axp\Calculator\Node;

use Axp\Calculator\ExecutionContextInterface;
use Axp\Calculator\UnsupportedOperatorException;
use Axp\Calculator\NodeInterface;

/**
 * Class OperatorNode
 * @package Axp\Calculator\Node
 */
class OperatorNode implements NodeInterface
{
    /**
     * @var string
     */
    private $operator;
    /**
     * @var NodeInterface
     */
    private $leftNode;
    /**
     * @var NodeInterface
     */
    private $rightNode;

    /**
     * @param string $operator
     * @param NodeInterface $leftNode
     * @param NodeInterface $rightNode
     */
    public function __construct($operator, NodeInterface $leftNode, NodeInterface $rightNode)
    {
        $this->operator = $operator;
        $this->leftNode = $leftNode;
        $this->rightNode = $rightNode;
    }

    /**
     * @param ExecutionContextInterface $context
     * @throws UnsupportedOperatorException
     * @return double
     */
    public function evaluate(ExecutionContextInterface $context)
    {
        switch ($this->operator) {
            case '+':
                return $this->leftNode->evaluate($context) + $this->rightNode->evaluate($context);
                break;
            case '-':
                return $this->leftNode->evaluate($context) - $this->rightNode->evaluate($context);
                break;
            case '*':
                return $this->leftNode->evaluate($context) * $this->rightNode->evaluate($context);
                break;
            case '/':
                return $this->leftNode->evaluate($context) / $this->rightNode->evaluate($context);
                break;
            case '=':
                return ($this->leftNode->evaluate($context) - $this->rightNode->evaluate($context)
                    ) <= 0.000000000000000 ? 1 : 0;
                break;
            default:
                throw new UnsupportedOperatorException('Unsupported calculator operator');
                break;
        }
    }

    /**
     * @return NodeInterface
     */
    public function getLeftNode()
    {
        return $this->leftNode;
    }

    /**
     * @return NodeInterface
     */
    public function getRightNode()
    {
        return $this->rightNode;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }
}
